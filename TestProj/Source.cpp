#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>

using namespace std;

//Win check function.
int Check(int *n, vector<int*> v){
	bool answer = 1;
	for (int* at : v){
		if (*(at) != *(n)){
			answer = 0;
		}
	}
	return answer;
}

void main(){

	//Board position initialize.
	vector<int> Board;
	for (int i = 0; i < 9; i++){
		Board.push_back(0);
	}

	//Board winning connections initialize. Unlimited Blade Works does have great animations...
	vector<vector<int*>> Connections;
	Connections.push_back({ &(Board.at(0)), &(Board.at(1)), &(Board.at(2)) });
	Connections.push_back({ &(Board.at(3)), &(Board.at(4)), &(Board.at(5)) });
	Connections.push_back({ &(Board.at(6)), &(Board.at(7)), &(Board.at(8)) });
	Connections.push_back({ &(Board.at(0)), &(Board.at(3)), &(Board.at(6)) });
	Connections.push_back({ &(Board.at(1)), &(Board.at(4)), &(Board.at(7)) });
	Connections.push_back({ &(Board.at(2)), &(Board.at(5)), &(Board.at(8)) });
	Connections.push_back({ &(Board.at(0)), &(Board.at(4)), &(Board.at(8)) });
	Connections.push_back({ &(Board.at(2)), &(Board.at(4)), &(Board.at(6)) });

	//Flag initialize (stop: keep going?	player: who's turn?	lastmove:board position of last move)
	bool stop = false;
	int player = 1;
	int *lastmove = NULL;

	//start position of goto.
	start:

	//loop
	while (!stop){
		system("CLS"); //Clear console hehehe so kewl I never knew how.
		stop = true;
		cout << "Verson 0.0.0: Make sure you ONLY type integers. You have been warned." << endl;

		//Print board.
		int count = 0;
		for (int i : Board){
			count++;
			if (i == 0){
				cout << (count) << " [   ] ";
			}
			else if (i == 1){
				cout << (count) << " [ x ] ";
			}
			else if (i == 2){
				cout << (count) << " [ o ] ";
			}
			if (count % 3 == 0){
				cout << endl;
			}
		}

		//answer integer for draw or win.
		int answer;

		//Check for draw.
		bool draw = true;
		for (int n : Board){
			if (n == 0){
				draw = false;
			}
		}
		if (draw){
			cout << "Draw! Rematch? (1/0)" << endl;
			cin >> answer;
			if (answer == 1){
				stop = false;
				lastmove = NULL;
				for (int i = 0; i < Board.size(); i++){
					Board.at(i) = 0;
				}
			}
			goto start;
		}

		//Check for win condition.
		if (lastmove != NULL){
			for (vector<int*> V : Connections){
				//cout << *(V.at(0)) << " " << *(V.at(1)) << " " << *(V.at(2)) << " " << *(lastmove) << endl; //Debug
				for (int* I : V){
					if (I == lastmove){

						//This happens is win is confirmed. If rematch, clear board then keep going. If not, exit.
						if (Check(lastmove, V) == 1){
							if (player == 1){
								cout << "Player 2 wins! Rematch? (1/0)" << endl;
								cin >> answer;
								if (answer == 1){
									stop = false;
									lastmove = NULL;
									for (int i = 0; i < Board.size(); i++){
										Board.at(i) = 0;
									}
								}
							}
							else{
								cout << "Player 1 wins! Rematch? (1/0)" << endl;
								cin >> answer;
								if (answer == 1){
									stop = false;
									lastmove = NULL;
									for (int i = 0; i < Board.size(); i++){
										Board.at(i) = 0;
									}
								}
							}
							goto start;
						}
					}
				}
			}
		}
		
		//Promt player to make move.
		cout << "Player " << player << ", it's your move!" << endl;
		int lol;
		bool valid = false;
		while (!valid){
			std::cin >> lol;
			if ((0 < lol) && (lol < 10)){
				if (Board.at(lol - 1) == 0){
					Board.at(lol - 1) = player;
					lastmove = &(Board.at(lol-1));
					valid = true;
				}
				else{
					cout << "Invalid placement. Try again!" << endl;
				}
			}
			else{
				cout << "Invalid move. Try again!" << endl;
			}
		}

		//Set next turn to be for the other player.
		if (player == 1){
			player = 2;
		}
		else if (player == 2){
			player = 1;
		}
		stop = false;
	}
}

